package pawelJankowski.grocery_store.service.DTO;

import org.springframework.stereotype.Component;
import pawelJankowski.grocery_store.repository.Product;
import pawelJankowski.grocery_store.repository.StoreWarehouse;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class StoreWarehouseTransformer {
    public StoreWarehouseDTO getDTO(StoreWarehouse storeWarehouse) {
        StoreWarehouseDTO storeWarehouseDTO = new StoreWarehouseDTO();
        storeWarehouseDTO.setName(storeWarehouse.getName());
        if (storeWarehouse.getProducts() != null) {
            storeWarehouseDTO.setProducts(storeWarehouse.getProducts().stream()
                    .map(product -> product.getName())
                    .collect(Collectors.toList()));
        }
        return storeWarehouseDTO;
    }

    public StoreWarehouse getModel(StoreWarehouseDTO storeWarehouseDTO) {
        StoreWarehouse storeWarehouse = new StoreWarehouse();
        storeWarehouse.setName(storeWarehouseDTO.getName());
        if (storeWarehouseDTO.getProducts() != null) {
            List<Product> productList = storeWarehouseDTO.getProducts().stream().map(p -> {
                Product product = new Product();
                product.setName(p);
                return product;
            }).collect(Collectors.toList());
            storeWarehouse.setProducts(productList);
        }
        return storeWarehouse;
    }
}
