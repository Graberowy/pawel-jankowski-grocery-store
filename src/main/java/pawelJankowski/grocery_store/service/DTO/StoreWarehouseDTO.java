package pawelJankowski.grocery_store.service.DTO;




import java.util.List;

public class StoreWarehouseDTO {
    private String name;
    private List<String> products;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }
}
