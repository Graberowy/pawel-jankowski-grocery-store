package pawelJankowski.grocery_store.service.DTO;

import org.springframework.stereotype.Component;
import pawelJankowski.grocery_store.repository.Product;
import pawelJankowski.grocery_store.repository.StoreWarehouse;

@Component
public class ProductTransformer {
    public ProductDTO getDTO(Product product){
        ProductDTO productDTO = new ProductDTO();
        productDTO.setCurrency(product.getCurrency());
        productDTO.setPrice(product.getPrice());
        productDTO.setName(product.getName());
        productDTO.setQuantity(product.getQuantity());
        if(product.getWarehouseName() != null) {
            productDTO.setWarehouseName(product.getWarehouseName().getName());
        }
        return productDTO;
    }
    public Product getModel(ProductDTO productDTO){
        Product product = new Product();
        product.setQuantity(productDTO.getQuantity());
        product.setCurrency(productDTO.getCurrency());
        product.setPrice(productDTO.getPrice());
        product.setName(productDTO.getName());
        if(productDTO.getWarehouseName() != null){
            StoreWarehouse storeWarehouse = new StoreWarehouse();
            storeWarehouse.setName(productDTO.getWarehouseName());
            product.setWarehouseName(storeWarehouse);
        }
        return product;
    }
}
