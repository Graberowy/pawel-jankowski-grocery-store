package pawelJankowski.grocery_store.service.DTO;


import java.math.BigDecimal;
import java.util.Objects;

public class ProductDTO {

    private String name;
    private BigDecimal price;
    private String currency;
    private Integer quantity;
    private String warehouseName;

    public ProductDTO() {
    }

    public ProductDTO(String name, BigDecimal price, String currency, Integer quantity, String warehouseName) {
        this.name = name;
        this.price = price;
        this.currency = currency;
        this.quantity = quantity;
        this.warehouseName = warehouseName;
    }

    public ProductDTO(String name, String currency) {
        this.name = name;
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDTO that = (ProductDTO) o;
        return Objects.equals(name, that.name) && Objects.equals(price, that.price) && Objects.equals(currency, that.currency) && Objects.equals(quantity, that.quantity) && Objects.equals(warehouseName, that.warehouseName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, currency, quantity, warehouseName);
    }
}
