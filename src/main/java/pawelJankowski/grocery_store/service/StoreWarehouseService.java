package pawelJankowski.grocery_store.service;

import org.springframework.stereotype.Service;
import pawelJankowski.grocery_store.repository.Product;
import pawelJankowski.grocery_store.repository.ProductRepository;
import pawelJankowski.grocery_store.repository.StoreWarehouse;
import pawelJankowski.grocery_store.repository.StoreWarehouseRepository;
import pawelJankowski.grocery_store.service.DTO.StoreWarehouseDTO;
import pawelJankowski.grocery_store.service.DTO.StoreWarehouseTransformer;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class StoreWarehouseService {

    private final StoreWarehouseRepository storeWarehouseRepository;
    private final ProductRepository productRepository;
    private final StoreWarehouseTransformer storeWarehouseTransformer;

    public StoreWarehouseService(StoreWarehouseRepository storeWarehouseRepository, ProductRepository productRepository, StoreWarehouseTransformer storeWarehouseTransformer) {
        this.storeWarehouseRepository = storeWarehouseRepository;
        this.productRepository = productRepository;
        this.storeWarehouseTransformer = storeWarehouseTransformer;
    }


    public List<StoreWarehouseDTO> getAllStoreWarehouses() {
        return storeWarehouseRepository.findAll().stream()
                .map(product -> storeWarehouseTransformer.getDTO(product))
                .collect(Collectors.toList());
    }

    public StoreWarehouseDTO addNewStoreWarehouse(StoreWarehouseDTO storeWarehouseDTO) {
        StoreWarehouse storeWarehouse = storeWarehouseTransformer.getModel(storeWarehouseDTO);
        storeWarehouseRepository.findById(storeWarehouse.getName())
                .ifPresent((o) -> {
                    throw new IllegalArgumentException();
                });
        List<Product> productsFromDb = storeWarehouse.getProducts().stream().map(product ->
                productRepository.findById(product.getName()).get()).collect(Collectors.toList());
        storeWarehouse.setProducts(productsFromDb);
        storeWarehouseRepository.save(storeWarehouse);
        productsFromDb.forEach(product -> {
            product.setWarehouseName(storeWarehouse);
            productRepository.save(product);
        });
        return storeWarehouseTransformer.getDTO(storeWarehouse);
    }
    public StoreWarehouseDTO removeStoreWarehouse(String storeWarehouseName) {
        StoreWarehouse storeWarehouse = storeWarehouseRepository.findById(storeWarehouseName).orElseThrow(() -> {
            throw new NoSuchElementException();
        });
        if(storeWarehouse.getProducts() != null){
            List<Product> productsFromDb = storeWarehouse.getProducts().stream().map(product ->
                    productRepository.findById(product.getName()).get()).collect(Collectors.toList());
            productsFromDb.forEach(product -> {
                product.setWarehouseName(null);
                productRepository.save(product);
            });
        };
        storeWarehouseRepository.delete(storeWarehouse);
        return storeWarehouseTransformer.getDTO(storeWarehouse);
    }
}
