package pawelJankowski.grocery_store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pawelJankowski.grocery_store.repository.Product;
import pawelJankowski.grocery_store.repository.ProductRepository;
import pawelJankowski.grocery_store.repository.StoreWarehouse;
import pawelJankowski.grocery_store.repository.StoreWarehouseRepository;
import pawelJankowski.grocery_store.service.DTO.ProductDTO;
import pawelJankowski.grocery_store.service.DTO.ProductTransformer;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductTransformer productTransformer;
    private final StoreWarehouseRepository storeWarehouseRepository;


    @Autowired
    public ProductService(ProductRepository productRepository, ProductTransformer productTransformer, StoreWarehouseRepository storeWarehouseRepository) {
        this.productRepository = productRepository;
        this.productTransformer = productTransformer;
        this.storeWarehouseRepository = storeWarehouseRepository;
    }

    public List<ProductDTO> getAllProducts(String direction) {
        Sort sort;
        if ("asc".equals(direction)) {
            sort = Sort.by(Sort.Direction.ASC, "name");
        } else if ("desc".equals(direction)) {
            sort = Sort.by(Sort.Direction.DESC, "name");
        } else {
            sort = Sort.unsorted();
        }

        return productRepository.findAll(sort).stream()
                .map(product -> productTransformer.getDTO(product))
                .collect(Collectors.toList());
    }

    public ProductDTO getById(String name) {
        return productTransformer.getDTO(
                productRepository.findById(name)
                        .orElseThrow(() -> {
                            throw new NoSuchElementException();
                        })
        );
    }

    public ProductDTO addNewProduct(ProductDTO productDTO) {
        Product product = productTransformer.getModel(productDTO);
        productRepository.findById(product.getName())
                .ifPresent((o) -> {
                    throw new IllegalArgumentException();
                });
        if (product.getWarehouseName() != null && product.getWarehouseName().getName() != null) {
            StoreWarehouse storeWarehouse = storeWarehouseRepository.findById(product.getWarehouseName().getName())
                    .orElseThrow(() -> {
                        throw new NoSuchElementException();
                    });
            product.setWarehouseName(storeWarehouse);
        }
        return productTransformer.getDTO(productRepository.save(product));
    }

    public ProductDTO deleteProduct(String productName) {
        Product product = productRepository.findById(productName).orElseThrow(() -> {
            throw new NoSuchElementException();
        });
        productRepository.delete(product);
        return productTransformer.getDTO(product);
    }

    public ProductDTO update(ProductDTO updatedProductDTO) {
        Product updatedProduct = productTransformer.getModel(updatedProductDTO);
        Product product = productRepository.findById(updatedProduct.getName()).orElseThrow(() -> {
            throw new NoSuchElementException();
        });
        StoreWarehouse storeWarehouseProduct = product.getWarehouseName();
        if (updatedProduct.getWarehouseName() != null && updatedProduct.getWarehouseName().getName().equals("None")) {
            product.setWarehouseName(null);
        } else if (updatedProduct.getWarehouseName() != null && !updatedProduct.getWarehouseName().getName().equals("None")) {
            product.setWarehouseName(storeWarehouseRepository.getById(updatedProduct.getWarehouseName().getName()));
        }
        if (updatedProduct.getPrice() != null) {
            product.setPrice(updatedProduct.getPrice());
        }
        if (updatedProduct.getQuantity() != null) {
            product.setQuantity(updatedProduct.getQuantity());
        }
        productRepository.save(product);
        if (storeWarehouseProduct != null && product.getWarehouseName() == null) {
            storeWarehouseProduct.getProducts().remove(product);
            storeWarehouseRepository.save(storeWarehouseProduct);
        } else if (storeWarehouseProduct != null && product.getWarehouseName() != null && !storeWarehouseProduct.getName().equals(product.getWarehouseName().getName())) {
            storeWarehouseProduct.getProducts().remove(product);
            storeWarehouseRepository.save(storeWarehouseProduct);
            product.getWarehouseName().getProducts().add(product);
            storeWarehouseRepository.save(product.getWarehouseName());
        }
        return productTransformer.getDTO(updatedProduct);
    }
}
