package pawelJankowski.grocery_store.service.initializer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pawelJankowski.grocery_store.repository.Product;
import pawelJankowski.grocery_store.repository.ProductRepository;
import pawelJankowski.grocery_store.repository.StoreWarehouse;
import pawelJankowski.grocery_store.repository.StoreWarehouseRepository;

import java.math.BigDecimal;

@Component
public class DBInitializer implements CommandLineRunner {
    private final ProductRepository productRepository;
    private final StoreWarehouseRepository storeWarehouseRepository;

    public DBInitializer(ProductRepository productRepository, StoreWarehouseRepository storeWarehouseRepository) {
        this.productRepository = productRepository;
        this.storeWarehouseRepository = storeWarehouseRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        StoreWarehouse storeWarehouse1 = new StoreWarehouse();
        storeWarehouse1.setName("Grocery");
        storeWarehouseRepository.save(storeWarehouse1);

        StoreWarehouse storeWarehouse2 = new StoreWarehouse();
        storeWarehouse2.setName("PolMat");
        storeWarehouseRepository.save(storeWarehouse2);

        Product product1 = new Product();
        product1.setName("carrot");
        product1.setPrice(BigDecimal.valueOf(1.21));
        product1.setCurrency("PLN");
        product1.setQuantity(52);
        product1.setWarehouseName(storeWarehouse1);
        productRepository.save(product1);

        Product product2 = new Product();
        product2.setName("tomato");
        product2.setPrice(BigDecimal.valueOf(1.99));
        product2.setCurrency("PLN");
        product2.setQuantity(91);
        product2.setWarehouseName(storeWarehouse1);
        productRepository.save(product2);

        Product product3 = new Product();
        product3.setName("cucumber");
        product3.setPrice(BigDecimal.valueOf(0.99));
        product3.setCurrency("PLN");
        product3.setQuantity(68);
        product3.setWarehouseName(storeWarehouse1);
        productRepository.save(product3);

        Product product4 = new Product();
        product4.setName("beetroot");
        product4.setPrice(BigDecimal.valueOf(0.88));
        product4.setCurrency("PLN");
        product4.setQuantity(156);
        product4.setWarehouseName(storeWarehouse1);
        productRepository.save(product4);

        Product product5 = new Product();
        product5.setName("cauliflower");
        product5.setPrice(BigDecimal.valueOf(2.14));
        product5.setCurrency("PLN");
        product5.setQuantity(79);
        product5.setWarehouseName(storeWarehouse1);
        productRepository.save(product5);

        Product product6 = new Product();
        product6.setName("orange");
        product6.setPrice(BigDecimal.valueOf(3.52));
        product6.setCurrency("PLN");
        product6.setQuantity(101);
        product6.setWarehouseName(storeWarehouse2);
        productRepository.save(product6);

        Product product7 = new Product();
        product7.setName("apple");
        product7.setPrice(BigDecimal.valueOf(2.43));
        product7.setCurrency("PLN");
        product7.setQuantity(566);
        product7.setWarehouseName(storeWarehouse2);
        productRepository.save(product7);

        Product product8 = new Product();
        product8.setName("broccoli");
        product8.setPrice(BigDecimal.valueOf(1.01));
        product8.setCurrency("PLN");
        product8.setQuantity(44);
        productRepository.save(product8);

        Product product9 = new Product();
        product9.setName("potato");
        product9.setPrice(BigDecimal.valueOf(0.55));
        product9.setCurrency("PLN");
        product9.setQuantity(210);
        productRepository.save(product9);

        Product product10 = new Product();
        product10.setName("sweet potato");
        product10.setPrice(BigDecimal.valueOf(5.25));
        product10.setCurrency("PLN");
        product10.setQuantity(89);
        productRepository.save(product10);
    }
}
