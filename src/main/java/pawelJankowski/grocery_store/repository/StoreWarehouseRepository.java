package pawelJankowski.grocery_store.repository;

import org.springframework.data.jpa.repository.JpaRepository;



public interface StoreWarehouseRepository extends JpaRepository<StoreWarehouse, String> {
}
