package pawelJankowski.grocery_store.repository;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class Product {
    @Id
    @NotNull
    @NotBlank
    @Size(min = 2, max = 15)
    private String name;
    @NotNull
    @PositiveOrZero
    private BigDecimal price;
    @NotNull
    @NotBlank
    @Size(min = 1, max = 4)
    private String currency;
    @NotNull
    @PositiveOrZero
    private Integer quantity;
    @ManyToOne
    private StoreWarehouse warehouseName;

    public Product() {
    }

    public Product(String name, String currency) {
        this.name = name;
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public StoreWarehouse getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(StoreWarehouse warehouseName) {
        this.warehouseName = warehouseName;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) && Objects.equals(price, product.price) && Objects.equals(currency, product.currency) && Objects.equals(quantity, product.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, currency, quantity);
    }
}
