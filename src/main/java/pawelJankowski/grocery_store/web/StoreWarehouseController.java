package pawelJankowski.grocery_store.web;

import org.springframework.web.bind.annotation.*;
import pawelJankowski.grocery_store.repository.StoreWarehouse;
import pawelJankowski.grocery_store.service.DTO.StoreWarehouseDTO;
import pawelJankowski.grocery_store.service.StoreWarehouseService;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/warehouse")
public class StoreWarehouseController {

    private final StoreWarehouseService storeWarehouseService;

    public StoreWarehouseController(StoreWarehouseService storeWarehouseService) {
        this.storeWarehouseService = storeWarehouseService;
    }

    @GetMapping
    public List<StoreWarehouseDTO> getAllWarehouses() {
        return storeWarehouseService.getAllStoreWarehouses();
    }

    @PostMapping
    public StoreWarehouseDTO addWarehouse(@RequestBody StoreWarehouseDTO storeWarehouse){
        return storeWarehouseService.addNewStoreWarehouse(storeWarehouse);
    }
    @DeleteMapping("/delete/{name}")
    void deleteStoreWarehouse(@PathVariable String name) {
        storeWarehouseService.removeStoreWarehouse(name);
    }
}
