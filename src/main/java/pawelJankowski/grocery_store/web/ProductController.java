package pawelJankowski.grocery_store.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pawelJankowski.grocery_store.service.DTO.ProductDTO;
import pawelJankowski.grocery_store.service.ProductService;

import java.util.List;
import java.util.NoSuchElementException;
@CrossOrigin
@RestController
@RequestMapping("/product")
class ProductController {
    private final ProductService productService;


    ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    List<ProductDTO> readAllProducts(@RequestParam(required = false) String direction) {
        return productService.getAllProducts(direction);
    }

    @GetMapping("/{name}")
    ProductDTO getByName(@PathVariable String name) {
        return productService.getById(name);
    }

    @PostMapping
    ProductDTO createProduct(@Validated @RequestBody ProductDTO toCreate) {
        return productService.addNewProduct(toCreate);
    }

    @DeleteMapping("/delete/{name}")
    void deleteProductByName(@PathVariable String name) {
        productService.deleteProduct(name);
    }

    @PutMapping("update/{name}")
    ProductDTO updateProductByName(@RequestBody ProductDTO toUpdate) {
        return productService.update(toUpdate);
    }

    @ExceptionHandler(value = NoSuchElementException.class)
    ResponseEntity<?> handleException(NoSuchElementException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    ResponseEntity<?> handleException(IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
