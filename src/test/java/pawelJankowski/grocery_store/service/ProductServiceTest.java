package pawelJankowski.grocery_store.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pawelJankowski.grocery_store.repository.Product;
import pawelJankowski.grocery_store.repository.ProductRepository;
import pawelJankowski.grocery_store.repository.StoreWarehouseRepository;
import pawelJankowski.grocery_store.service.DTO.ProductDTO;
import pawelJankowski.grocery_store.service.DTO.ProductTransformer;

import java.math.BigDecimal;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
class ProductServiceTest {
    @TestConfiguration
    static class TestProductServiceProvider{
        @Bean
        ProductService productService(ProductRepository productRepository, StoreWarehouseRepository storeWarehouseRepository){
            return new ProductService(productRepository, new ProductTransformer() ,storeWarehouseRepository);
        }
    }
    @Autowired
    private ProductService productService;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private StoreWarehouseRepository storeWarehouseRepository;

    @Test
    void when_send_get_request_with_valid_id_then_product_with_provided_id_should_be_returned() {
        //given
        Product product = new Product();
        product.setName("Tomato");
        product.setPrice(BigDecimal.valueOf(1.51));
        product.setQuantity(8);
        product.setCurrency("PLN");
        Mockito.when(productRepository.findById("Tomato")).thenReturn(Optional.of(product));

        //when
        ProductDTO productReturned = productService.getById("Tomato");

        //then
        assertEquals("PLN", productReturned.getCurrency());
        assertEquals("Tomato", productReturned.getName());
        assertEquals(BigDecimal.valueOf(1.51), productReturned.getPrice());
        assertEquals(8, productReturned.getQuantity());
    }

    @Test
    void when_send_get_request_with_invalid_id_then_not_found_exception_should_returned() {
        //given
        Mockito.when(productRepository.findById("Tomato")).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoSuchElementException.class, () -> {
            productService.getById("Tomato");
        });
    }
    @Test
    void when_send_post_request_with_non_existing_product_then_product_should_be_added_to_repo() {
        //given
        Product product = new Product();
        product.setName("Tomato");
        product.setPrice(BigDecimal.valueOf(1.51));
        product.setQuantity(8);
        product.setCurrency("PLN");
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName("Tomato");
        productDTO.setPrice(BigDecimal.valueOf(1.51));
        productDTO.setQuantity(8);
        productDTO.setCurrency("PLN");
        Mockito.when(productRepository.save(product)).thenReturn(product);

        //when
        ProductDTO productReturned = productService.addNewProduct(productDTO);

        //then
        assertEquals("PLN", productReturned.getCurrency());
        assertEquals("Tomato", productReturned.getName());
        assertEquals(BigDecimal.valueOf(1.51), productReturned.getPrice());
        assertEquals(8, productReturned.getQuantity());
    }

    @Test
    void when_send_post_request_with_existing_organization_then_exception_should_be_thrown() {
        //given
        Product product = new Product();
        product.setName("Tomato");
        product.setPrice(BigDecimal.valueOf(1.51));
        product.setQuantity(8);
        product.setCurrency("PLN");
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName("Tomato");
        productDTO.setPrice(BigDecimal.valueOf(1.51));
        productDTO.setQuantity(8);
        productDTO.setCurrency("PLN");
        Mockito.when(productRepository.findById("Tomato")).thenReturn(Optional.of(product));

        //when
        //then
        assertThrows(IllegalArgumentException.class, () -> {
            productService.addNewProduct(productDTO);
        });
    }

    @Test
    void when_send_delete_request_with_existing_product_name_then_organization_should_be_removed() {
        //given
        Product product = new Product();
        product.setName("Tomato");
        product.setPrice(BigDecimal.valueOf(1.51));
        product.setQuantity(8);
        product.setCurrency("PLN");
        Mockito.when(productRepository.findById("Tomato")).thenReturn(Optional.of(product));

        //when
        productService.deleteProduct("Tomato");

        //then
        Mockito.verify(productRepository).delete(product);
    }

    @Test
    void when_send_delete_request_with_non_existing_product_name_then_exception_should_be_thrown() {
        //given
        Mockito.when(productRepository.findById("Tomato")).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoSuchElementException.class, () -> {
            productService.deleteProduct("Tomato");
        });
    }

    @Test
    void when_send_get_request_to_get_product_list_in_asc_order_then_ordered_list_should_be_returned() {
        //given
        ArgumentCaptor<Sort> captor = ArgumentCaptor.forClass(Sort.class);

        //when
        productService.getAllProducts("asc");

        //then
        Mockito.verify(productRepository).findAll(captor.capture());
        assertTrue(captor.getValue().getOrderFor("name").isAscending());
    }
    @Test
    void when_send_get_request_to_get_product_list_in_dsc_order_then_ordered_list_should_be_returned() {
        //given
        ArgumentCaptor<Sort> captor = ArgumentCaptor.forClass(Sort.class);

        //when
        productService.getAllProducts("desc");

        //then
        Mockito.verify(productRepository).findAll(captor.capture());
        assertTrue(captor.getValue().getOrderFor("name").isDescending());
    }

    @Test
    void when_send_get_request_to_get_unsorted_product_list_then_unsorted_product_list_should_be_returned() {
        //given
        ArgumentCaptor<Sort> captor = ArgumentCaptor.forClass(Sort.class);

        //when
        productService.getAllProducts("");

        //then
        Mockito.verify(productRepository).findAll(captor.capture());
        assertTrue(captor.getValue().isUnsorted());
    }



}