package pawelJankowski.grocery_store.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import pawelJankowski.grocery_store.repository.Product;
import pawelJankowski.grocery_store.service.DTO.ProductDTO;
import pawelJankowski.grocery_store.service.ProductService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)
class ProductControllerTest {
    @MockBean
    private ProductService productService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void when_send_get_request_by_name_with_existing_product_name_then_product_should_be_returned() throws Exception {
        //given
        ProductDTO product = new ProductDTO();
        product.setName("Tomato");
        product.setPrice(BigDecimal.valueOf(1.51));
        product.setQuantity(8);
        product.setCurrency("PLN");
        Mockito.when(productService.getById("Tomato")).thenReturn(product);

        //when
        //then
        mockMvc.perform(get("/product/Tomato").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("Tomato")))
                .andExpect(jsonPath("$.price", equalTo(1.51)))
                .andExpect(jsonPath("$.quantity", equalTo(8)))
                .andExpect(jsonPath("$.currency", equalTo("PLN")));
    }

    @Test
    void when_send_get_request_by_name_which_is_not_exist_in_repo_then_not_found_error_should_be_returned() throws Exception {
        //given
        Mockito.when(productService.getById("Tomato")).thenThrow(NoSuchElementException.class);

        //when
        //then
        mockMvc.perform(get("/product/Tomato").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
    @Test
    void when_send_post_request_which_has_not_existing_product_then_product_should_be_returned_as_response() throws Exception {
        //given
        ProductDTO productDTO = new ProductDTO();
        productDTO.setName("Tomato");
        productDTO.setPrice(BigDecimal.valueOf(1.51));
        productDTO.setQuantity(8);
        productDTO.setCurrency("PLN");
        Mockito.when(productService.addNewProduct(productDTO)).thenReturn(productDTO);

        //when
        //then
        mockMvc.perform(post("/product/")
                .content("{\n" +
                        "  \"name\": \"Tomato\",\n" +
                        "  \"price\": 1.51,\n" +
                        "  \"currency\": \"PLN\",\n" +
                        "  \"quantity\": 8\n" +
                        "}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("Tomato")))
                .andExpect(jsonPath("$.price", equalTo(1.51)))
                .andExpect(jsonPath("$.quantity", equalTo(8)))
                .andExpect(jsonPath("$.currency", equalTo("PLN")));
    }

    @Test
    void when_send_post_request_which_has_organization_then_error_should_be_returned() throws Exception {
        //given
        ProductDTO product = new ProductDTO();
        product.setName("Tomato");
        product.setPrice(BigDecimal.valueOf(1.51));
        product.setQuantity(8);
        product.setCurrency("PLN");
        Mockito.when(productService.addNewProduct(product)).thenThrow(IllegalArgumentException.class);

        //when
        //then
        mockMvc.perform(post("/product/")
                .content("{\n" +
                        "  \"name\": \"Tomato\",\n" +
                        "  \"price\": 1.51,\n" +
                        "  \"currency\": \"PLN\",\n" +
                        "  \"quantity\": 8\n" +
                        "}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void when_send_get_request_with_asc_order_then_ordered_list_should_be_returned() throws Exception {
        //given
        Mockito.when(productService.getAllProducts("asc")).thenReturn(Arrays.asList(
                new ProductDTO("a", "a"),
                new ProductDTO("b", "b"),
                new ProductDTO("c", "c")
        ));

        //when
        //then
        mockMvc.perform(get("/product?direction=asc").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", equalTo("a")))
                .andExpect(jsonPath("$[1].name", equalTo("b")))
                .andExpect(jsonPath("$[2].name", equalTo("c")));
    }




}